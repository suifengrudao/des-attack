#include <iostream>
#include <vector>

#include "Test/qr_test.h"

using namespace std;

int main(){
	cout<<"**********************************test 1*****************************\n";
	OPENSSL::BIGNUM_SPACE::random_test_on_root_of_quadratic_residure(10);

	cout<<"\n\n\n\n**********************************test 2*****************************\n";
	OPENSSL::BIGNUM_SPACE::random_test_on_root_of_quadratic_residure(20);


	cout<<"\n\n\n\n**********************************test 3*****************************\n";
	OPENSSL::BIGNUM_SPACE::random_test_on_root_of_quadratic_residure(30);

	cout<<"\n\n\n\n**********************************test 4*****************************\n";
	OPENSSL::BIGNUM_SPACE::random_test_on_root_of_quadratic_residure(40);
	return 0;
}