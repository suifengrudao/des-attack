// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

#pragma once

/*
* utils.h
*
*/
#include <array>
#include <string>
#include <sstream>
#include <cstdint>
#include <map>
#include <sys/stat.h>
#include <sys/types.h>

#include <openssl/rand.h>
#include <openssl/err.h>

namespace ffse {
	constexpr size_t kKey_Size = 32;
	constexpr size_t kID_Size = 8;
    constexpr size_t kMap_Size = 40; // 8bytes for id; 32bytes for key;
    constexpr size_t Ind_Size = 4; // fid range from 0 to 2^32-1
    constexpr size_t kValue_Size = 44; // 8bytes for id; 32bytes for key; 31bits for ind; 1bits for op
    constexpr size_t kBlock_Size = 52; // a transfered block
    constexpr size_t id_counter_offset = 30000000;

    constexpr int CT = 2;
    // use for multi-thread communication
    constexpr int port_increase = 150;
    constexpr int max_TransferBlocks = 500000;

	typedef std::array<uint8_t, kID_Size>  kID_type;
	typedef std::array<uint8_t, kKey_Size>  kkey_type;
	typedef uint64_t fid_type;  // file id type

    class randomSketchGenerator{
    private:
        bool initialized;
        unsigned char key[32];
    public:
        randomSketchGenerator() {
          RAND_poll();
          initialized = true;
        }

        void reseed() {
          int written = RAND_bytes(key, sizeof(key));
          RAND_seed(key, written);
        }

        bool random_bytes(const size_t &byte_count, unsigned char* out)
        {
          int rc = RAND_bytes(out, byte_count);
          unsigned long err = ERR_get_error();

          if(rc != 0 && rc != 1) {
            fprintf(stderr, "Generating Random Bytes failed, err = 0x%lx\n", err);
            return false;
          }

          return true;
        }
    };

} // namespace ffse

/*
* files operation
*/
bool is_directory(const std::string& path);
bool create_directory(const std::string& path, mode_t mode);

/*
* load client local store file
*/
template <class MapClass>
bool parse_keyword_map(std::istream& in, MapClass& kw_map)
{
    std::string line, kw, index_string;

    while (std::getline(in, line)) {
        std::stringstream line_stream(line);

        if(!std::getline(line_stream, kw, '\t'))
        {
            return false;
        }
        if (!std::getline(line_stream,index_string)) {
            return false;
        }
        kw_map.insert(std::make_pair(kw,std::stoul(index_string, NULL, 16)));
    }
    return true;
}