/*************************************************************************
* m_test.h
* Check the properties of m-sequence
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/

/*
*   given a n-stage fsr, check whether it produce m_sequence
* 	i.e. with initial state as 0^n, it outputs 0^n forever
*		 with initial state as others, its periodic as 2^n-1
*
*		the maximum support tape is 32, we can improve this by subsitute unsigned int to big num
*/
#include <vector>
bool check_m_sequence_via_perioidic(unsigned int fsr, unsigned int n_stage);

/*
*   given a n-stage fsr, generate the m_sequence from random non-zeros initial states
*/
void gen_m_sequence(unsigned int fsr, unsigned int n_stage, std::vector<unsigned char>& m_sequence);

/*
*   check the properties of m sequence
*/
void check_m_sequence_properties(unsigned int n_stage, std::vector<unsigned char> m_sequence);