/*************************************************************************
* rappor_attack_test.h
* Check the properties of m-sequence
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/


/*
*   generate a permanent response from a Bloom filter B (a byte char) of size k using h hash functions
*		the flip probability is f 
*/
unsigned char* gen_permanent_response(const double f,unsigned char* B, unsigned int len_B, const unsigned int k);

/*
* Function to get no of set bits in binary representation of positive integer n 
*/
unsigned int countSetBits(unsigned char n);

static const unsigned int setBits_perByte[256] = {
	0 , 1 , 1 , 2 , 1 , 2 , 2 , 3 ,
	1 , 2 , 2 , 3 , 2 , 3 , 3 , 4 ,
	1 , 2 , 2 , 3 , 2 , 3 , 3 , 4 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	1 , 2 , 2 , 3 , 2 , 3 , 3 , 4 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	1 , 2 , 2 , 3 , 2 , 3 , 3 , 4 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	4 , 5 , 5 , 6 , 5 , 6 , 6 , 7 ,
	1 , 2 , 2 , 3 , 2 , 3 , 3 , 4 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	4 , 5 , 5 , 6 , 5 , 6 , 6 , 7 ,
	2 , 3 , 3 , 4 , 3 , 4 , 4 , 5 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	4 , 5 , 5 , 6 , 5 , 6 , 6 , 7 ,
	3 , 4 , 4 , 5 , 4 , 5 , 5 , 6 ,
	4 , 5 , 5 , 6 , 5 , 6 , 6 , 7 ,
	4 , 5 , 5 , 6 , 5 , 6 , 6 , 7 ,
	5 , 6 , 6 , 7 , 6 , 7 , 7 , 8   
};

/*
*   check the statistic of the generated permanent response
*/
void check_permanent_response_validity(const double f,unsigned char* B, unsigned int len_B, const unsigned int k);

/*
*   generate a instantaneous response from a permanent response PR of size k
*		the flip to 1 probability is q   when PR[i] is 1; p   when PR[i] is 0
*		the flip to 0 probability is 1-q when PR[i] is 1; 1-p when PR[i] is 0
*/
unsigned char* gen_instantaneous_response(const double q, const double p, unsigned char* PR, unsigned int len_B, const unsigned int k);

/*
*   check the statistic of the generated instantaneous response
*/
void check_instantaneous_response(const double f,unsigned char* B, const double q, const double p, unsigned char* PR, unsigned int len_B, const unsigned int k);
