/*************************************************************************
* des_test.cpp
* Check the functionality of des
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <set>
 
#include "Tools/hex.h"
#include "Crypto/des_system.h"
#include "Crypto/des_locl.h"
#include "Crypto/spr.h"
#include "Crypto/inverse_tools.h"
#include "Exceptions/Exceptions.h"
#include "Tools/utils.h"

#include "des_test.h"

using namespace std;

/*
*   sample code of des ecb
*/
void samples_des_ecb()
{
    /* 
    *   Input key then generate a encryption system
    */
    unsigned char key[8] = {0};
    DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::ecb, key, false);
    // DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::ecb);
    sys.get_key(key);
    printf("The key is:\t");
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(key + i));
    }
    printf("\n");


    /**********************************************************************************************************************
    *   encrypt the data 
    */
    cout<<"*********************************************test the DES_ECB_ENCRYPT functionality******************************************\n";
    string data = "12345678123456781234567812345678"; 
    int in_len = strlen(data.c_str());
    int out_len = 0;
    unsigned char *cipher = 0;
    cipher = sys.DES_ECB_ENCRYPT((unsigned char *)data.c_str(), in_len, out_len, true);
    // output
    printf("Before encryption:\t");
    for (int i = 0; i < out_len; i++) {
        printf("%.2X", *(cipher + i));
    }
    printf("\n");

    /***************************************************************************************************************************** 
    *   decrypt the data 
    */
    in_len = out_len;
    unsigned char *plaintext = 0;
    plaintext = sys.DES_ECB_ENCRYPT(cipher, in_len, out_len, false);
    // output
    cout<<"After decryption:\t"<<string((char*) plaintext, out_len)<<endl;
    cout<<"*********************************************test complete*****************************************************************\n";


    cout<<"*********************************************test the DES_ECB_BLOCK_ENCRYPT functionality**********************************\n";
    unsigned char in[8] = {0,1,2,3,4,5,6,7};
    unsigned char out[8];


    sys.DES_ECB_BLOCK_ENCRYPT(in, out, true);
    printf("After encryption:\t");
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(out + i));
    }
    printf("\n");


    sys.DES_ECB_BLOCK_ENCRYPT(out, in, false);
    printf("After decryption:\t");
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(in + i));
    }
    printf("\n");
    cout<<"*********************************************test complete*****************************************************************\n";


    /*
    * recycle part
    */
    delete cipher;
    delete plaintext;
}

/*
*   sample code of simulator des ecb
*/
void samples_simulator_des_ecb(){
    unsigned char key[8] = {0};
    DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::simpler_ecb, key, false);
    // DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::simpler_ecb, true);

    cout<<"*********************************************test the DES_ECB_BLOCK_ENCRYPT functionality**********************************\n";
    unsigned char in[8] = {0,1,2,3,4,5,6,7};
    unsigned char out[8] = {0};
    unsigned char subkey[8] = {0};

    sys.get_subkey(1,subkey);
    printf("The round %d subkey is:\t",1);
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(subkey + i));
    }
    printf("\n");

    sys.TEST_DES_ECB_BLOCK_ENCRYPT(in, out, true, true, true, 16);

    printf("After encryption:\t");
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(out + i));
    }
    printf("\n");

    sys.TEST_DES_ECB_BLOCK_ENCRYPT(out, in, false, true, true, 16);
    printf("After decryption:\t");
    for (int i = 0; i < 8; i++) {
        printf("%.2X", *(in + i));
    }
    printf("\n");
    cout<<"*********************************************test complete*****************************************************************\n";
}


/*
*   test validation of simulator des
*/
bool test_simulator_des_ecb(int count){
    if(count < 1){
        count = 1;
    }
    unsigned char key[8];
    unsigned char in[8];
    unsigned char out[8];
    unsigned char out_simulator[8];
    DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::ecb,true);
    sys.get_key(key);
    DES_SYSTEM_SPACE::DES_SYSTEM sys_simulator(DES_SYSTEM_SPACE::simpler_ecb, key, true);


    ffse::randomSketchGenerator rsg;
    rsg.reseed();
    bool ret,pass_enc, pass_dec, pass;

    pass_enc = true;
    pass_dec = true;
    pass = true;

    for(int i=0; i<count; i++){
        ret = rsg.random_bytes(8, (unsigned char *)in);
        if(!ret){
            throw std::runtime_error("Error: cannot generate random bytes");
        }

        sys.DES_ECB_BLOCK_ENCRYPT(in, out, true);
        sys_simulator.TEST_DES_ECB_BLOCK_ENCRYPT(in, out_simulator, true, true, true, 16);

        ret = false;
        for(int i=0; i<8; i++){
            ret |= (out[i]-out_simulator[i]);
        }

        pass_enc &= (!ret);
    }

    if(pass_enc){
        cout<<"the encryption functionality of simulator is pass\n";
    }else{
        cout<<"the encryption functionality of simulator is failed\n";
    }


    for(int i=0; i<count; i++){
        ret = rsg.random_bytes(8, (unsigned char *)in);
        if(!ret){
            throw std::runtime_error("Error: cannot generate random bytes");
        }

        sys.DES_ECB_BLOCK_ENCRYPT(in, out, false);
        sys_simulator.TEST_DES_ECB_BLOCK_ENCRYPT(in, out_simulator, false, true, true, 16);

        ret = false;
        for(int i=0; i<8; i++){
            ret |= (out[i]-out_simulator[i]);
        }

        pass_dec &= (!ret);
    }

    if(pass_dec){
        cout<<"the decryption functionality of simulator is pass\n";
    }else{
        cout<<"the decryption functionality of simulator is failed\n";
    }



    pass &= pass_enc;
    pass &= pass_dec;
    return pass;
}


bool test_simulator_des_subkeys(int count){
    if(count < 1){
        count = 1;
    }

    bool pass = true;

    for(int c=0; c<count; c++){
        unsigned char key[8];
        unsigned char subkeys[16][8];
        DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::ecb,true);
        sys.get_key(key);
        sys.get_subkey(subkeys);


        DES_SYSTEM_SPACE::KEY_Simulator key_simulator(key);
        unsigned char simulator_key[8];
        unsigned char simulator_subkeys[16][8];
        key_simulator.get_key(simulator_key);
        key_simulator.get_subkey(simulator_subkeys);

        bool ret = false;
        for(int i=0; i<8; i++){
            ret |= (key[i]-simulator_key[i]);
        }

        for(int i=0; i<16; i++){
            for(int j=0; j<8; j++){
                ret |= (subkeys[i][j]-simulator_subkeys[i][j]);
            }
        }

        pass &= (! ret);
    }
    
    if(pass){
        cout<<"the subkey generation functionality of simulator is pass\n";
    }

    return pass;
}

void bits_index_output(ostream& os, unsigned int& x){
    int loop = sizeof(x)*8;

    unsigned int tmp = x;
    bool odd;
    for(int i=0; i<loop; i++){
        odd = tmp & 1;
        tmp = tmp >> 1;
        if(odd){
            os<<i<<"  ";
        }
    }
}
void simple_analysis_S_BOX(){
    // the index of the lowest bit is 0
    unsigned int S_BOX[8][64];
    for(int i=0; i<8; i++){
        memcpy(S_BOX[i], DES_SPtrans[i], 64*sizeof(unsigned int));
    }

    
    // for(int i=0; i<8; i++){
    //     cout<<"S_BOX "<<i<<"\tset bits at :\t";

    //     unsigned int tmp = 0;
    //     for(int j=0; j<64; j++){
    //         tmp |= S_BOX[i][j];
    //     }

    //     bits_index_output(cout, tmp);
    //     cout<<endl;
    // }

    unsigned int b;
    unsigned int output_xor;
    unsigned int bit;
    unsigned int tmp;

    set<unsigned int> xor_distribution_map[8][64][16];

    for(int No_BOX = 0; No_BOX<8; No_BOX++){
        for(unsigned int input_xor=0; input_xor<64; input_xor++){
            for(unsigned int a=0; a<64; a++){
                b = a^input_xor;
                output_xor = S_BOX[No_BOX][a] ^ S_BOX[No_BOX][b];

                tmp = 0;
                for(int i=0; i<4; i++){
                    bit = ((output_xor >> DES_S_index[No_BOX][i]) & 1);
                    tmp |= bit<<i;
                }
                output_xor = tmp;
                xor_distribution_map[No_BOX][input_xor][output_xor].insert(a);
                xor_distribution_map[No_BOX][input_xor][output_xor].insert(b);
            }
        }
    }
}


void parse_skb_item(const unsigned int& x, int No_BOX, unsigned int& out){
    unsigned int bit;
    unsigned int tmp;

    tmp = 0;
    for(int i=0; i<6; i++){
        bit = ((x >> DES_skb_index[No_BOX][i]) & 1);
        tmp |= bit<<i;
    }
    out = tmp;
}

void simple_analysis_skb(){
    // the index of the lowest bit is 0
    unsigned int S_BOX[8][64];
    unsigned int small_box[8][64];
    unsigned int inv_box[8][64];
    for(int i=0; i<8; i++){
        memcpy(S_BOX[i], des_skb[i], 64*sizeof(unsigned int));
        memset(small_box[i], 0, 64*sizeof(unsigned int));
    }


    // for(int i=0; i<8; i++){
    //     cout<<"skb_BOX "<<i<<"\tset bits at :\t";

    //     unsigned int tmp = 0;
    //     for(int j=0; j<64; j++){
    //         tmp |= S_BOX[i][j];
    //     }

    //     bits_index_output(cout, tmp);
    //     cout<<endl;
    // }

    

    for(int No_BOX=0; No_BOX<8; No_BOX++){
        for(int i=0; i<64; i++){
            parse_skb_item(S_BOX[No_BOX][i], No_BOX, small_box[No_BOX][i]);
        }
    }

    for(int No_BOX=0; No_BOX<8; No_BOX++){
        for(int i=0; i<64; i++){
            inv_box[No_BOX][small_box[No_BOX][i]] = i;
        }
    }


    int offset = 0;

    for(int No_BOX=0; No_BOX<8; No_BOX++){
        offset = 0;
        cout<<"{\n";
        for(int i=0; i<8; i++){
            for(int j=0; j<8; j++){
                cout<<inv_box[No_BOX][offset++]<<", ";
            }
            cout<<endl;
        }
        cout<<"},\n";
    }
    
}

void subkey_differ_test(int round){
    unsigned int origin[2];
    unsigned int recover[2];
    unsigned int xor_result[2] = {0};

    const int bench_size = 200;
    for(int i=0; i<bench_size; i++){
        DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::simpler_ecb);
        unsigned char key[8] = {0};
        sys.get_key(key);

        unsigned int round_subkey[2] = {0};
        sys.get_subkey(0, round_subkey);
        DES_SYSTEM_SPACE::KEY_Simulator key_simulator(key);

        key_simulator.tmp_experiment(round, origin, recover);
        xor_result[0] |= origin[0]^recover[0];
        xor_result[1] |= origin[1]^recover[1];
    }
    
    cout<<"test bench_size "<<bench_size<<"\t(";
    bits_index_output(cout, xor_result[0]);
    cout<<"\t,\t";
    bits_index_output(cout, xor_result[1]);
    cout<<")\n";
}