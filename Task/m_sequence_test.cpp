#include <iostream>
#include <vector>
#include "Test/m_test.h"

using namespace std;

/*
* some irreducible polynomial
*
*	when n=6 std::vector<unsigned char> v = {1, 0, 0, 0, 0, 1};
*			 std::vector<unsigned char> v = {1, 0, 1, 1, 0, 1};
*
*	when n=7 std::vector<unsigned char> v = {1, 0, 0, 0, 0, 0, 1};
*
*	when n=8 std::vector<unsigned char> v = {1, 0, 0, 0, 1, 1, 0, 1};
*/
int main(){
	/*
	*	check the polynomial whether irreducible
	*/
	// bool pass = check_m_sequence_via_perioidic(fsr, n);
	// if(pass){
	// 	cout<<"the polynomial can generate m-sequence\n";
	// }else{
	// 	cout<<"the polynomial cannot generate m-sequence\n";
	// }

	{
		cout<<"******************************start 6 stage test ******************************\n\n";
		int n = 6;
		std::vector<unsigned char> v = {1, 0, 0, 0, 0, 1};//pos[0] is the monomial with degree n, monomial with degree 0 is constant 1
		cout<<"the 6 stage LFSR is:\t x^6 + x + 1\n"; 
		//In the integer, pos[n] is the monomial with degree n, monomial with degree 0 is constant 1 
		unsigned int fsr = 0;
		for(int i=0; i<n; i++){
			fsr = ((fsr<<1) | v[i]);
		}

		std::vector<unsigned char> m_sequence;
		gen_m_sequence(fsr, n, m_sequence);
		check_m_sequence_properties(n, m_sequence);
		cout<<"******************************end 6 stage test ******************************\n\n";
	}

	{
		cout<<"******************************start 7 stage test ******************************\n\n";
		int n = 7;
		cout<<"the 7 stage LFSR is:\t x^7 + x + 1\n"; 
		std::vector<unsigned char> v = {1, 0, 0, 0, 0, 0, 1};//pos[0] is the monomial with degree n, monomial with degree 0 is constant 1 
		//In the integer, pos[n] is the monomial with degree n, monomial with degree 0 is constant 1 
		unsigned int fsr = 0;
		for(int i=0; i<n; i++){
			fsr = ((fsr<<1) | v[i]);
		}

		std::vector<unsigned char> m_sequence;
		gen_m_sequence(fsr, n, m_sequence);
		check_m_sequence_properties(n, m_sequence);
		cout<<"******************************end 7 stage test ******************************\n\n";
	}

	{
		cout<<"******************************start 8 stage test ******************************\n\n";
		int n = 8;
		cout<<"the 8 stage LFSR is:\t x^8 + x^4 + x^3 + x + 1\n"; 
		std::vector<unsigned char> v = {1, 0, 0, 0, 1, 1, 0, 1};//pos[0] is the monomial with degree n, monomial with degree 0 is constant 1 
		//In the integer, pos[n] is the monomial with degree n, monomial with degree 0 is constant 1 
		unsigned int fsr = 0;
		for(int i=0; i<n; i++){
			fsr = ((fsr<<1) | v[i]);
		}

		std::vector<unsigned char> m_sequence;
		gen_m_sequence(fsr, n, m_sequence);
		check_m_sequence_properties(n, m_sequence);
		cout<<"******************************end 8 stage test ******************************\n\n";
	}
	



	return 0;
}