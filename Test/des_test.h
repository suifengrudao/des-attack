/*************************************************************************
* des_test.h
* Check the functionality of des
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include <iostream>

/*
*   sample code of des ecb
*/
void samples_des_ecb();

void bits_index_output(std::ostream& os, unsigned int& x);
/*
*   sample code of simulator des ecb
*/
void samples_simulator_des_ecb();


/*
*   test validation of simulator des
*/
bool test_simulator_des_ecb(int bench_size);


/*
*   test validation of simulator subkey systems
*/
bool test_simulator_des_subkeys(int bench_size);


/*
*   simple analysis S-BOX in spr
*/
void simple_analysis_S_BOX();

/*
*   simple analysis skb box in des_local
*/
void simple_analysis_skb();


void subkey_differ_test(int round);