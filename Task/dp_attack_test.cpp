/*************************************************************************
* .cpp
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include <iostream>
#include <cstring>
#include <math.h>

#include "../Test/rappor_attack_test.h"

using namespace std;

int main()
{
    /*
    *   Set the global parameters
    *       f is the flipping possiblity from bloom filter to permanent random response, we set it as 0.5
    *       k is the size of the bloom filter, we set it as 128
    *       q is the flipping possiblity 
    */
    double f = 0.5;
    double q = 0.75;
    double p = 0.5; 
    unsigned int k = 128;
    unsigned int len_B = ceil(double(k) / (sizeof(unsigned char)<<3));
    unsigned char* B = new unsigned char[len_B];

    // generate the fake bloom filter
    memset(B, 0x00, len_B);

    // generate response candidate 1
    unsigned char* pr1 = gen_permanent_response(f, B, len_B, k);

    // if you need, you can check the statistic of the PR
    // check_permanent_response_validity(f, B, len_B, k);

    // generate report candidate 1
    unsigned char* ir1 = gen_instantaneous_response(q, p, pr1, len_B, k);
    
    // if you need, you can check the statistic of the IR
    // check_instantaneous_response(f, B, q, p, pr1, len_B, k);


    // recycle and exit
    delete pr1;
    delete ir1;
    return 0;
}