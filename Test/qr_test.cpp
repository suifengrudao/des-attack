/*************************************************************************
* qr_test.cpp
* Some auxiliary functiona and check functionality for the functionality of 
*	calculating the root of the quardratic residure of a given mod prime
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include "qr_test.h"
#include "Exceptions/Exceptions.h"


#include <iostream>

namespace OPENSSL{
	namespace BIGNUM_SPACE{
		BIGNUM * generate_normal_prime(unsigned int least_bits_length){
			BIGNUM *p = BN_new();
			BN_CTX *ctx = BN_CTX_new();
			int ret = 0;

			while(ret != 1){
				ret = BN_generate_prime_ex(p, least_bits_length, false, NULL, NULL, NULL);
				if(ret != 1){
					continue;
				}
				ret = BN_is_prime_ex(p, BN_prime_checks ,ctx ,NULL);
			}
			

			BN_CTX_free(ctx);
			return p;
		}

		BIGNUM*	generate_quadratic_residure(const BIGNUM* mod_p){
			BIGNUM *c = BN_new();
			BIGNUM *r = BN_new();
			BN_CTX *ctx = BN_CTX_new();
			BIGNUM *one = BN_new();
			BIGNUM *two = BN_new();
			int ret = 1;
			BN_one(one);
			BN_set_word(two, 2);


			while(ret != 0){
				BN_pseudo_rand_range(c, mod_p);
				BN_mod_sub(r, mod_p, one, mod_p, ctx);
				BN_div(r, NULL, r, two, ctx);
				BN_mod_exp(r, c, r, mod_p, ctx);
				ret = BN_cmp(r, one);
				
			}


			BN_CTX_free(ctx);
			BN_free(r);
			BN_free(one);
			BN_free(two);
			return c;
		}

		BIGNUM*	generate_non_quadratic_residure(const BIGNUM* mod_p){
			BIGNUM *c = BN_new();
			BIGNUM *r = BN_new();
			BN_CTX *ctx = BN_CTX_new();
			BIGNUM *one = BN_new();
			BIGNUM *two = BN_new();
			int ret = 0;
			BN_one(one);
			BN_set_word(two, 2);


			while(ret != 1){
				BN_pseudo_rand_range(c, mod_p);
				BN_mod_sub(r, mod_p, one, mod_p, ctx);
				BN_div(r, NULL, r, two, ctx);
				BN_mod_exp(r, c, r, mod_p, ctx);
				ret = BN_cmp(r, one);		
			}


			BN_CTX_free(ctx);
			BN_free(r);
			BN_free(one);
			BN_free(two);
			return c;
		}

		void get_order_of_2sylow_element(const BIGNUM* mod_p, const BIGNUM* b, BIGNUM* order){
			BIGNUM *one = BN_new();
			BIGNUM *r = BN_dup(b);
			BN_CTX *ctx = BN_CTX_new();
			BN_one(one);
			BN_zero(order);
			int ret = BN_cmp(r, one);

			while(ret != 0){
				BN_mod_mul(r, r, r, mod_p, ctx);
				BN_add(order, order, one);
				ret = BN_cmp(r, one);
			}

			BN_free(one);
			BN_free(r);
			BN_CTX_free(ctx);
		}

		void extract_r_and_s(const BIGNUM* mod_p, BIGNUM* r, BIGNUM* s){
			/*
			*	initialization
			*/
			BIGNUM *zero = BN_new();
			BN_zero(zero);
			BIGNUM *one = BN_new();
			BN_one(one);
			BIGNUM *two = BN_new();
			BN_set_word(two, 2);
			BN_CTX *ctx = BN_CTX_new();
			BN_zero(r);
			BIGNUM *a = BN_new();
			BIGNUM *b = BN_new();
			
			/*
			*	functionality
			*/
			BN_mod_sub(a, mod_p, one, mod_p, ctx);
			BN_copy(s, a);
			BN_div(a, b, a, two, ctx);
			while(BN_cmp(b, zero) == 0){
				BN_add(r, r, one);
				BN_copy(s, a);
				BN_div(a, b, a, two, ctx);
			}


			/*
			*	free the allocated memory
			*/
			BN_free(zero);
			BN_free(one);
			BN_free(two);
			BN_free(a);
			BN_free(b);
			BN_CTX_free(ctx);
		}

		void bn_dec_printf(BIGNUM * a)
		{
			char *p = BN_bn2dec(a);
			printf("%s\n", p);
			OPENSSL_free(p);
		}

		void bn_hex_printf(BIGNUM * a)
		{
			char *p = BN_bn2hex(a);
			printf("0x%s\n", p);
			OPENSSL_free(p);
		}

		void random_test_on_root_of_quadratic_residure(unsigned int least_bits_length){
			// some variable initialization
			BN_CTX *ctx = BN_CTX_new();
			BIGNUM *one = BN_new();
			BIGNUM *tmp_var1 = BN_new();
			BN_one(one);
			BIGNUM *two = BN_new();
			BN_set_word(two, 2);
			BIGNUM *zero = BN_new();
			BN_zero(zero);

			/*
			*	a simple test
			*/
			// BIGNUM *p = BN_new();
			// BIGNUM *a = BN_new();
			// BN_set_word(p, 113);
			// BN_set_word(a, 7);

			// generate prime p and quadratic residure c
			BIGNUM *p = OPENSSL::BIGNUM_SPACE::generate_normal_prime(least_bits_length);
			BIGNUM *a = OPENSSL::BIGNUM_SPACE::generate_quadratic_residure(p);

			std::cout<<"the task find the quadratic residure \t";
			OPENSSL::BIGNUM_SPACE::bn_dec_printf(a);

			std::cout<<"in the field of the prime p with "<<BN_num_bits(p)<<" bits \t";
			OPENSSL::BIGNUM_SPACE::bn_dec_printf(p);

			// get r and s such that p-1 = 2^r * s
			BIGNUM *r = BN_new();
			BIGNUM *s = BN_new();
			OPENSSL::BIGNUM_SPACE::extract_r_and_s(p, r, s);

			// generate the non quadratic residure n and 2-sylow subgroup generator m
			BIGNUM *n = OPENSSL::BIGNUM_SPACE::generate_non_quadratic_residure(p);
			BN_set_word(n, 3);
			BIGNUM *m = BN_new();
			BN_mod_exp(m, n, s, p, ctx);

			// initialize pre_b, pre_x and order of pre_b
			BIGNUM *pre_b = BN_new();
			BIGNUM *order_b = BN_new();
			BIGNUM *pre_x = BN_new();
			BN_mod_exp(pre_b, a, s, p, ctx);
			BN_add(tmp_var1, s, one);
			BN_div(tmp_var1, NULL, tmp_var1, two, ctx);
			BN_mod_exp(pre_x, a, tmp_var1, p, ctx);
			OPENSSL::BIGNUM_SPACE::get_order_of_2sylow_element(p, pre_b, order_b);

			/*
			*	iteration until get one root of the quadratic residure
			*/
			while(BN_cmp(order_b, zero) != 0){
				// compute adjusted b
				BN_sub(tmp_var1, r, order_b);
				BN_mod_exp(tmp_var1, two, tmp_var1, p, ctx);
				BN_mod_exp(tmp_var1, m, tmp_var1, p, ctx);
				BN_mod_mul(pre_b, tmp_var1, pre_b, p, ctx);

				// compute the updated x
				BN_sub(tmp_var1, r, order_b);
				BN_sub(tmp_var1, tmp_var1, one);
				BN_mod_exp(tmp_var1, two, tmp_var1, p, ctx);
				BN_mod_exp(tmp_var1, m, tmp_var1, p, ctx);
				BN_mod_mul(pre_x, tmp_var1, pre_x, p, ctx);

				// compute the order of the adjusted b
				OPENSSL::BIGNUM_SPACE::get_order_of_2sylow_element(p, pre_b, order_b);
			}

			std::cout<<"the first root of the quadratic residure: x is \t";
			OPENSSL::BIGNUM_SPACE::bn_dec_printf(pre_x);
			BN_mod_sub(pre_x, zero, pre_x, p, ctx);
			std::cout<<"the second root of the quadratic residure: x is \t";
			OPENSSL::BIGNUM_SPACE::bn_dec_printf(pre_x);


			
			// free the allocated memory
			BN_free(p);
			BN_free(a);
			BN_free(r);
			BN_free(n);
			BN_free(s);
			BN_free(zero);
			BN_free(one);
			BN_free(two);
			BN_free(tmp_var1);
			BN_free(order_b);
			BN_CTX_free(ctx);
		}
	}
}