/*************************************************************************
* rappor_attack_test.cpp
* Check the properties of m-sequence
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include "rappor_attack_test.h"

#include <cassert>
#include <iostream>
#include <random>
#include <math.h>

using std::cout;
using std::endl;
unsigned char* gen_permanent_response(const double f, unsigned char* B, unsigned int len_B, const unsigned int k){
    unsigned int bits_per_unsigned_char = (sizeof(unsigned char)<<3);
	assert((len_B*bits_per_unsigned_char-k+1)>0);
    unsigned char* PR = new unsigned char[len_B];

	// obtain an random double [0,1] generator
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(0.0, 1.0);

    // length of the complete block
    unsigned int len_cb = floor(double(k) /bits_per_unsigned_char);
    double half_f = f/2;
    unsigned char origin, tmp;
    double flipping = 0.0;
    
    // generate the full 1 mask
    unsigned char mask = 0;
    for(unsigned int i=0; i<bits_per_unsigned_char; i++){
        mask = (mask<<1);
        mask = mask | 1;
    }

    for(unsigned int i=0; i<len_cb; i++){
        origin = B[i];
        // cout<<"origin:\t"<<(unsigned int)origin<<endl;
        for(unsigned int j=0; j<bits_per_unsigned_char; j++){
            flipping = dis(gen);
            // cout<<"("<<i*bits_per_unsigned_char+j<<")\tflipping:\t"<<flipping<<"\n";
            // with probability of f flipping the bit
            if(flipping < f){
                // with probability of f/2 flipping the bit to 1
                if(flipping < half_f){
                    tmp = (1<<j);
                    origin |= tmp;
                    // cout<<"flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }else{
                    // with probability of f/2 flipping the bit to 0
                    tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                    origin &= tmp;
                    // cout<<"flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }
            }
        }
        PR[i] = origin;
    }

    origin = B[len_B-1];
    for(unsigned int j = bits_per_unsigned_char-1; j >= bits_per_unsigned_char-(k-len_cb*bits_per_unsigned_char); j--){
        flipping = dis(gen);
        // cout<<"("<<j<<")\tflipping:\t"<<flipping<<"\n";
        // with probability of f flipping the bit
        if(flipping < f){
            // with probability of f/2 flipping the bit to 1
            if(flipping < half_f){
                tmp = (1<<j);
                origin |= tmp;
                // cout<<"flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }else{
                // with probability of f/2 flipping the bit to 0
                tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                origin &= tmp;
                // cout<<"flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }
        }
        PR[len_B-1] = origin;
    }
    

    return PR;
}

unsigned int countSetBits(unsigned char n){ 
    unsigned int count = 0; 
    while (n) 
    { 
        count += n & 1; 
        n >>= 1; 
    } 
    return count; 
} 
/*
*   suppose there are X's 1 and Y's 0 in B, satisfying X + Y = k
*   there should have 0.5fk + (1-f)X's 1 and 0.5fk + (1-f)Y's 0 in PR on average
*/
void check_permanent_response_validity(const double f,unsigned char* B, unsigned int len_B, const unsigned int k){
    assert((len_B*8-k+1)>0);
    unsigned int len_cb = floor(double(k) / 8);
    unsigned int X = 0 , Y = 0;
    unsigned int tmp;
    for(unsigned int i=0; i<len_B; i++){
        tmp = setBits_perByte[B[i]];
        X += tmp;
        Y += (8 - tmp);
    }
    tmp = k-len_cb*8;
    if(tmp){
        Y -= (8-tmp);
    }
    assert( X+Y == k);

    unsigned int trails = 1024; 
    cout<<"the number of trails tested is "<<trails<<endl;

    unsigned char* PR;
    unsigned int x = 0, y = 0;
    unsigned int n1 = 0, n0 = 0; // number of 1; number of 0
    for(unsigned int cnt=0; cnt<trails; cnt++){
        x = y = 0;
        PR = gen_permanent_response(f, B, len_B, k);

        for(unsigned int i=0; i<len_B; i++){
            tmp = setBits_perByte[PR[i]];
            x += tmp;
            y += (8 - tmp);
        }
        tmp = k-len_cb*8;
        if(tmp){
            y -= (8-tmp);
        }
        assert( x+y == k);
        n1 += x;
        n0 += y;
        // recycle
        delete PR;
    }

    cout<<"[*Experimental*]  The number of 1 is:\t "<< ((double)n1)/trails << ":\t and number of 0 is:\t "<< ((double)n0)/trails <<endl;
    cout<<"[*Statistics*]    The number of 1 is:\t "<< (0.5*f*k + (1-f)*X) << ":\t and number of 0 is:\t "<< (0.5*f*k + (1-f)*Y)<<endl;
}
/*
*   generate a instantaneous response from a permanent response PR of size k
*       the flip to 1 probability is q   when PR[i] is 1; p   when PR[i] is 0
*       the flip to 0 probability is 1-q when PR[i] is 1; 1-p when PR[i] is 0
*/
unsigned char* gen_instantaneous_response(const double q, const double p, unsigned char* PR, unsigned int len_B, const unsigned int k){
    unsigned int bits_per_unsigned_char = (sizeof(unsigned char)<<3);
    assert((len_B*bits_per_unsigned_char-k+1)>0);
    unsigned char* IR = new unsigned char[len_B];

    // obtain an random double [0,1] generator
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(0.0, 1.0);

    // length of the complete block
    unsigned int len_cb = floor(double(k) /bits_per_unsigned_char);
    unsigned char origin, tmp;
    double flipping = 0.0;

    // generate the full 1 mask
    unsigned char mask = 0;
    for(unsigned int i=0; i<bits_per_unsigned_char; i++){
        mask = (mask<<1);
        mask = mask | 1;
    }

    for(unsigned int i=0; i<len_cb; i++){
        origin = PR[i];
        // cout<<"origin:\t"<<(unsigned int)origin<<endl;
        for(unsigned int j=0; j<bits_per_unsigned_char; j++){
            flipping = dis(gen);
            // cout<<"("<<i*bits_per_unsigned_char+j<<")\tflipping:\t"<<flipping<<"\n";
            tmp = (origin>>j) & 1;
            // PR[j] is 1
            if(tmp){
                if(flipping<q){
                    tmp = (1<<j);
                    origin |= tmp;
                    // cout<<"q flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }else{
                    tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                    origin &= tmp;
                    // cout<<"1-q flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }
            }else{
                if(flipping<p){
                    tmp = (1<<j);
                    origin |= tmp;
                    // cout<<"p flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }else{
                    tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                    origin &= tmp;
                    // cout<<"1-p flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
                }
            }
        }
        IR[i] = origin;
    }

    origin = PR[len_B-1];
    for(unsigned int j = bits_per_unsigned_char-1; j >= bits_per_unsigned_char-(k-len_cb*bits_per_unsigned_char); j--){
        flipping = dis(gen);
        // cout<<"("<<i*bits_per_unsigned_char+j<<")\tflipping:\t"<<flipping<<"\n";
        tmp = (origin>>j) & 1;
        // PR[j] is 1
        if(tmp){
            if(flipping<q){
                tmp = (1<<j);
                origin |= tmp;
                // cout<<"q flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }else{
                tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                origin &= tmp;
                // cout<<"1-q flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }
        }else{
            if(flipping<p){
                tmp = (1<<j);
                origin |= tmp;
                // cout<<"p flipping to 1:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }else{
                tmp = (((mask<<(j+1))) | ((unsigned char)(mask<<(bits_per_unsigned_char - j)) >> (bits_per_unsigned_char - j)));
                origin &= tmp;
                // cout<<"1-p flipping to 0:\t"<<(unsigned int)origin<<endl<<endl<<endl;
            }
        }
        IR[len_B-1] = origin;
    }

    return IR;
}


/*
*   suppose there are X's 1 and Y's 0 in B, satisfying X + Y = k
*   there should have 0.5fk + (1-f)X's 1 and 0.5fk + (1-f)Y's 0 in PR on average
*   there should have 0.5fk(p+q) + (1-f)(Xq + Yp)'s 1 and 0.5fk(2-p-q) + (1-f)[X(1-q) + Y(1-p)]'s 0 in IR on average
*/
void check_instantaneous_response(const double f,unsigned char* B, const double q, const double p, unsigned char* PR, unsigned int len_B, const unsigned int k){
    assert((len_B*8-k+1)>0);
    unsigned int len_cb = floor(double(k) / 8);
    unsigned int X = 0 , Y = 0;
    unsigned int tmp;
    for(unsigned int i=0; i<len_B; i++){
        tmp = setBits_perByte[B[i]];
        X += tmp;
        Y += (8 - tmp);
    }
    tmp = k-len_cb*8;
    if(tmp){
        Y -= (8-tmp);
    }
    assert( X+Y == k);

    unsigned int trails = 1024; 
    cout<<"the number of trails tested is "<<trails<<endl;

    unsigned char* IR;
    unsigned int x = 0, y = 0;
    unsigned int n1 = 0, n0 = 0; // number of 1; number of 0
    for(unsigned int cnt=0; cnt<trails; cnt++){
        x = y = 0;
        IR = gen_instantaneous_response(q, p, PR, len_B, k);

        for(unsigned int i=0; i<len_B; i++){
            tmp = setBits_perByte[IR[i]];
            x += tmp;
            y += (8 - tmp);
        }
        tmp = k-len_cb*8;
        if(tmp){
            y -= (8-tmp);
        }
        assert( x+y == k);
        n1 += x;
        n0 += y;
        // recycle
        delete IR;
    }

    cout<<"[*Experimental*]  The number of 1 is:\t "<< ((double)n1)/trails << ":\t and number of 0 is:\t "<< ((double)n0)/trails <<endl;
    cout<<"[*Statistics*]    The number of 1 is:\t "<< (0.5*f*k*(p+q) + (1-f)*(X*q + Y*p)) << ":\t and number of 0 is:\t "<< (0.5*f*k*(2-p-q) + (1-f)*(X*(1-q) + Y*(1-p)))<<endl;
}
