/*************************************************************************
* .cpp
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include <string>
#include <cstring>
#include <sys/time.h>
 
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Tools/hex.h"
#include "Crypto/des_system.h"
#include "Crypto/des_locl.h"
#include "Test/des_test.h"
using namespace std;


int main()
{
    // cout<<"****************************************Show the analysis Tools******************************************\n";


    /*
    *   run the function simple_analysis_S_BOX() can test and output the xor_distribution_map
    *   which output the information of the
             (input_xor => output_xor) :=> {possible S-box input }
    */
    //  void simple_analysis_S_BOX();


    /*
    *   run the function inverse_subkeys(int round, unsigned int subkey[2], unsigned int result[2])
    *   which input the round number; round subkeys; and inverse the 48-bits of 56-bits subkeys
    *       inverse the functionality of PC2 and PC1
    */
    //  void inverse_subkeys(int round, unsigned int subkey[2], unsigned int result[2]);

    /*
    *   run the function subkey_differ_test(int round)
    *   which output the information of the 
    *       position of the 8 bits which dropped from the 56-bits to the 48-bits round subkeys
    */
    //  void subkey_differ_test(int round);



    /*
    *   Below are the sample code for the 1-round and 3-round DES attack
    *   Actually, they have the same attack computational complexity
    *       1) first generate the random key
    *       2) attack the S-box using the xor distribution map and recover the 48-bits round key
    *       3) inverse and brutely guess the rest 8 bits of the 56-bits key
    */  
    int round = 1;
    {
        cout<<"****************************************round 1 attack******************************************\n";
        unsigned char key[8];
        unsigned char simulation_key[8];
        
        DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::simpler_ecb, true);
        sys.get_key(key);
        printf("origin key is:\t");
        for (int i = 0; i < 8; i++) {
            printf("%.2X", *(key + i));
        }
        printf("\n");

        DES_SYSTEM_SPACE::DES_Cracker cracker(&sys);
        bool ret = cracker.attack_ecb_des(round, simulation_key);
        if(ret){
            printf("simulation key is:\t");
            for (int i = 0; i < 8; i++) {
                printf("%.2X", *(simulation_key + i));
            }
            printf("\n");
        }else{
            cout<<"can not recover the origin key, cracking failed\n";
        }

        cout<<"\n\n";
    }
	
    round = 3;
    {
        cout<<"****************************************round 3 attack******************************************\n";
        unsigned char key[8];
        unsigned char simulation_key[8];
        
        DES_SYSTEM_SPACE::DES_SYSTEM sys(DES_SYSTEM_SPACE::simpler_ecb, true);
        sys.get_key(key);
        printf("origin key is:\t");
        for (int i = 0; i < 8; i++) {
            printf("%.2X", *(key + i));
        }
        printf("\n");

        DES_SYSTEM_SPACE::DES_Cracker cracker(&sys);
        bool ret = cracker.attack_ecb_des(round, simulation_key);
        if(ret){
            printf("simulation key is:\t");
            for (int i = 0; i < 8; i++) {
                printf("%.2X", *(simulation_key + i));
            }
            printf("\n");
        }else{
            cout<<"can not recover the origin key, cracking failed\n";
        }
    }


    return 0;
}