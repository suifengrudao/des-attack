

## Pre-requisites
+   installing openssl 1.1.1b
+   mkdir folder build if it doesn't exist

#  Des attack on 1-round and 3-round

## functionality
+    Code to support the class post-work "Modern Cryptographic 2019"
+    Attack the source code of openssl 1.1.1b ecb mode des encryption(Round reduction)

## Run the test file
+   Compile 'make des-attack.x -j 4'
+   and Run './build/des-attack.x'


#  LFSR m-squence task
## functionality
+    Code to support the class post-work "Modern Cryptographic 2019"
+    Construct the primitive polynomial degree ranging from 6 to 8, then construct the corresponding external linear feedback shift register
+    initial with the non-zero state and test the period of the generated sequence whether satisfying the m sequence's period, i.e. 2^n-1
+    test the m sequence properties such as the 0-1 balance, length of run and autocorrelation function 

## Run the test file
+   Compile 'make m-sequence.x -j 4'
+   and Run './build/m-sequence.x'


#  solve the Root of the quadratic residure
## functionality
+    given the specific prime p and quadratic residure a, get the solution root of the quadratic residure x

## Run the test file
+   Compile 'make quadratic-residure.x -j 4'
+   and Run './build/quadratic-residure.x'
