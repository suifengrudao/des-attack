# (C) 2018 University of Bristol. See License.txt


include CONFIG

TOOLS = $(patsubst %.cpp,%.o,$(wildcard Tools/*.cpp))

Exceptions = $(patsubst %.cpp,%.o,$(wildcard Exceptions/*.cpp))

Crypto = $(patsubst %.cpp,%.o,$(wildcard Crypto/*.cpp))

TEST = $(patsubst %.cpp,%.o,$(wildcard Test/*.cpp))

COMPLETE = $(TOOLS) $(Exceptions) $(Crypto) $(TEST)



# used for dependency generation
OBJS = $(COMPLETE)
DEPS := $(OBJS:.o=.d)

-include $(DEPS)

%.o: %.cpp
	$(CXX) $(CFLAGS) -MMD -c -o $@ $<


# make order
clean:
	-rm */*.o *.o */*.d *.d */*.x *.x core.* *.a 

dp-attack.x: Task/dp_attack_test.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) Task/dp_attack_test.cpp -o build/dp-attack.x $(COMPLETE) $(LDLIBS)

des-attack.x: Task/des_attack_test.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) Task/des_attack_test.cpp -o build/des-attack.x $(COMPLETE) $(LDLIBS)

m-sequence.x: Task/m_sequence_test.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) Task/m_sequence_test.cpp -o build/m-sequence.x $(COMPLETE) $(LDLIBS)


quadratic-residure.x :Task/quadratic_residure_test.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) Task/quadratic_residure_test.cpp -o build/quadratic-residure.x $(COMPLETE) $(LDLIBS)
