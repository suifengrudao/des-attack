/*************************************************************************
* des_system.h
*
* DES crypto system
* 64 bits key; the plaintext are padded to times of 64 bits
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/

#pragma once

#include <openssl/des.h>
#include <set>

namespace DES_SYSTEM_SPACE{
	enum DES_SYSTEM_mode {invalid, simpler_ecb, ecb};

	class DES_SYSTEM{
	private:
		DES_key_schedule key_schedule;
		unsigned char key[8];

		/*
		* encryption mode
		*/
		DES_SYSTEM_mode mode;
	public:
		/*
		* Create an instance with random generated key
		*	the mode was been set invalid and the instance is not avaliable to use 
		*/
		DES_SYSTEM();

		/*
		* Create an instance with random generated key with specific mode
		*/
		DES_SYSTEM(const DES_SYSTEM_mode& _mode, bool key_checked = false);

		bool set_key(unsigned char _key[8], bool key_checked = false);

		/*
		* Create an instance with specific key and mode
		*/
		DES_SYSTEM(const DES_SYSTEM_mode& _mode, unsigned char _key[8], bool key_checked = false);

		/*
		* encrypt with ecb mode
		*/
		unsigned char* DES_ECB_ENCRYPT(unsigned char* in, int& in_len, int& out_len, bool is_enc = true);

		// this functionality just encrypt one block
		void DES_ECB_BLOCK_ENCRYPT(unsigned char in[8], unsigned char out[8], bool is_enc = true);

		// this functionality is for attacking experiment
		void TEST_DES_ECB_BLOCK_ENCRYPT(unsigned char in[8], unsigned char out[8], bool is_enc = true, bool ip_covered = true, bool f_permutated = true,int F_round = 16);

		/*
		* auxiliary function
		*/
		void get_key(unsigned char out[8]);
		void get_key(unsigned int out[2]);

		void get_subkey(int round, unsigned char out[8]);
		void get_subkey(int round, unsigned int  out[2]);
		void get_S_BOX_subkey(int round, unsigned int out[8]);
		void get_subkey(unsigned char out[16][8]);
	};


	class KEY_Simulator{
	private:
		unsigned char key[8];
		// 16 rounds; 2 subkeys each rounds
		unsigned int subkeys[32];

		void set_subkeys();
	public:
		KEY_Simulator(unsigned char _key[8]);

		/*
		* auxiliary function
		*/
		void get_key(unsigned char out[8]);

		void get_subkey(int round, unsigned char out[8]);
		void get_subkey(unsigned char out[16][8]);

		void tmp_experiment(int round ,unsigned int origin[2], unsigned int recover[2]);
	};

	class DES_Cracker
	{
	private:
		std::set<unsigned int> xor_distribution_map[8][64][16];
		DES_SYSTEM* victim;

		void load_xor_distribution_map();

		void parse_xor_output(unsigned int x, unsigned int out_xor[8]);
		void parse_xor_input(const unsigned int& x, unsigned int in_xor[8]);
	public:
		DES_Cracker(DES_SYSTEM* _victim);
		~DES_Cracker() {}

		bool attack_ecb_des(int round, unsigned char simulation_key[8]);

		bool attack_1round_ecb_des(unsigned char simulation_key[8]);
		bool attack_3round_ecb_des(unsigned char simulation_key[8]);
	};


	void inverse_subkeys(int round, unsigned int subkey[2], unsigned int result[2]);
	void parse_skb_output(unsigned int x, unsigned int y,unsigned int out_xor[8]);
}
