/*************************************************************************
* m_test.cpp
* Check the properties of m-sequence
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/
#include "m_test.h"

#include <math.h>
#include <stdlib.h>
#include <time.h> 
#include <string.h>

#include <iostream>

using namespace std;

bool check_m_sequence_via_perioidic(unsigned int fsr, unsigned int n_stage){
	unsigned int n = n_stage;
	unsigned int n_tape = 0;
	unsigned int cnt = 0;
	unsigned int mask = pow(2,n)-1;
	srand((unsigned)time(NULL)); 
	unsigned int states = rand() % mask;

	unsigned int a,b;

	bool* period_sq = new bool[int(pow(2,n))];
	int* fsr_mark = new int[n_stage];
	memset(period_sq, false, sizeof(bool) * pow(2,n));
	memset(fsr_mark, -1, sizeof(int) * n_stage);
	

	// conver the fsr from unsigned int type to index-type
	a = fsr;
	for(unsigned int i=0; i<n_stage; i++){
		if ((a & 1)){	
			fsr_mark[n_tape ++] = i;
		}
		a = (a>>1);
	}

	while(! period_sq[states]){
		//pre state
		period_sq[states] = true;
		cnt ++;

		a = states & fsr; // extract the tap bits
		b = 1; //initial the output bit
		for(unsigned int i=0; i<n_tape; i++){
			b ^= ((a >> fsr_mark[i]) & 1);
		}
		b ^= 1; //eliminate the mask and get the output bit

		//post state
		states = ((states>>1) | (b<<(n_stage-1)));
	}

	delete period_sq;
	delete fsr_mark;

	if(cnt == mask){
		return true;
	}else{
		return false;
	}
}

void gen_m_sequence(unsigned int fsr, unsigned int n_stage, std::vector<unsigned char>& m_sequence){
	unsigned int n = n_stage;
	unsigned int n_tape = 0;
	unsigned int cnt = 0;
	unsigned int mask = pow(2,n)-1;
	srand((unsigned)time(NULL)); 
	unsigned int states = rand() % mask;


	unsigned int a,b;

	bool* period_sq = new bool[int(pow(2,n))];
	int* fsr_mark = new int[n_stage];
	memset(period_sq, false, sizeof(bool) * pow(2,n));
	memset(fsr_mark, -1, sizeof(int) * n_stage);
	m_sequence.resize(mask);
	

	// conver the fsr from unsigned int type to index-type
	a = fsr;
	for(unsigned int i=0; i<n_stage; i++){
		if ((a & 1)){	
			fsr_mark[n_tape ++] = i;
		}
		a = (a>>1);
	}

	while(! period_sq[states]){
		//pre state
		period_sq[states] = true;

		a = states & fsr; // extract the tap bits
		b = 1; //initial the output bit
		for(unsigned int i=0; i<n_tape; i++){
			b ^= ((a >> fsr_mark[i]) & 1);
		}
		b ^= 1; //eliminate the mask and get the output bit
		m_sequence[cnt] = b;

		//post state
		states = ((states>>1) | (b<<(n_stage-1)));
		cnt ++;
	}

	delete period_sq;
	delete fsr_mark;
}

void check_m_sequence_properties(unsigned int n_stage, std::vector<unsigned char> m_sequence){
	unsigned int n_zero = 0;
	unsigned int n_one = 0;
	unsigned int m_len = m_sequence.size();
	bool b;

	// output the m-sequence
	cout<<"output the "<<n_stage<<" stages m sequence:\n";
	for(unsigned int i=0; i<m_len; i++){
		b = m_sequence[i];
		cout << b; // output the m-sequence

		//check the 0-1 balance
		if(b){
			n_one ++;
		}else{
			n_zero ++;
		}
	}
	cout<<endl;


	// check the 0-1 balance
	cout<<"\nthe m sequence has "<<n_zero<<" zeroes and "<<n_one<<" ones so it ";
	if((n_one == pow(2,n_stage-1)) && (n_zero == pow(2,n_stage-1)-1)){
		cout<<"pass the 0-1 balance check\n";
	}else{
		cout<<"doesn't pass the 0-1 balance check\n";
	}

	// check the number of separate run sequence
	unsigned int cnt = 0;
	unsigned int run_count[2*n_stage];
	memset(run_count, 0, sizeof(unsigned int) * (2*n_stage)); //even for 0 and odd for 1
	//locate the head first
	b = m_sequence[0];
	while(b == m_sequence[cnt++]){
	}
	unsigned int start_pos = cnt-1;

	cnt = 1;
	b = 1-b;
	for(unsigned int i = 1; i<m_len+1; i++){
		if(b == m_sequence[(i + start_pos) % m_len]){
			cnt ++;
		}else{
			//cout<<"run "<<b<<" with length of "<<cnt<<endl;
			run_count[2* (cnt-1) +b]++;
			b = 1-b;
			cnt = 1;
		}
	}
	cout<<"\noutput the sequence run properties:\n";
	for(unsigned int i=0; i<n_stage; i++){
		cout<<"0 run of length "<<(i+1)<<" has :\t"<<run_count[2*i]<<"\n";
		cout<<"1 run of length "<<(i+1)<<" has :\t"<<run_count[2*i + 1]<<"\n";
	}

	//	check the autocorrelation properties
	int u[m_len];
	int v[m_len];
	int auto_correlation_value[m_len];
	memset(u, 0, sizeof(int) * m_len);
	memset(auto_correlation_value, 0, sizeof(int) * m_len);

	for(unsigned int i=0; i<m_len; i++){
		if(m_sequence[i]){
			u[i] = -1;
		}else{
			u[i] = 1;
		}
	}

	cout<<"\noutput the correlation value properties, the form of which is (shift,auto_correlation_value)\n";
	for(unsigned int tau=0; tau<m_len; tau++){
		// calculate the v[i] first
		memset(v, 0, sizeof(int) * m_len);
		for(unsigned int i=0; i<m_len; i++){
			if(m_sequence[(i + tau) % m_len]){
				v[i] = -1;
			}else{
				v[i] = 1;
			}
		}

		// calculate the correlation function
		int cnt = 0;
		for(unsigned int i=0; i<m_len; i++){
			cnt += u[i]*v[i];
		}

		auto_correlation_value[tau] = cnt;
	}

	for(unsigned int tau=0; tau<m_len; tau++){
		cout<<"("<<tau<<","<<auto_correlation_value[tau]<<")\t";
	}
	cout<<endl;
}