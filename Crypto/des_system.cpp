/*************************************************************************
* des_system.cpp
*
* DES crypto system
* 64 bits key; the plaintext are padded to times of 64 bits
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/

#include "des_system.h"
#include "Exceptions/Exceptions.h"
#include "Tools/utils.h"
#include "math.h"


#include "des_locl.h"
#include "spr.h"
#include "inverse_tools.h"
#include "Test/des_test.h"

#include <time.h>
#include <cstring>
#include <iostream>

using std::cout;
using std::endl;
using std::set;

namespace DES_SYSTEM_SPACE{
	/*
	* Create an instance with random generated key
	*	the mode was been set invalid and the instance is not avaliable to use 
	*/
	DES_SYSTEM::DES_SYSTEM(){
		mode = invalid;
	}

	/*
	* Create an instance with random generated key with specific mode
	*/
	DES_SYSTEM::DES_SYSTEM(const DES_SYSTEM_mode& _mode, bool key_checked){
		mode = _mode;

		// initial the random number generator
		ffse::randomSketchGenerator rsg;
		rsg.reseed();
		int ret = 1;

		// randomly generate the weak key bytes
		ret = rsg.random_bytes(8, (unsigned char *)key);
		if(!ret){
			throw std::runtime_error("Key Generated Error: cannot generate random bytes");
		}

		// generate the key structure
		if(!key_checked){
			DES_set_key_unchecked((const_DES_cblock*)key, &key_schedule);
		}else{
			// generate the strong keys
			DES_random_key((DES_cblock*) key);
			DES_set_key_checked((const_DES_cblock*)key, &key_schedule);
		}
	}

	/*
	* Create an instance with specific key and mode
	*/
	DES_SYSTEM::DES_SYSTEM(const DES_SYSTEM_mode& _mode, unsigned char _key[8], bool key_checked){
		mode = _mode;
		memcpy(key, _key, 8);

		// implies the system is for attacking experiment
		if(mode == invalid){
			throw std::runtime_error(" invalid mode instance\n");
		}

		if(!key_checked){
			DES_set_key_unchecked((const_DES_cblock*)key, &key_schedule);
		}else{
			int ret = DES_set_key_checked((const_DES_cblock*)key, &key_schedule);
			switch(ret){
				case -1:
					throw std::runtime_error("Key Generated Error: the key passed is not of odd parity");
					break;
				case -2:
					throw std::runtime_error("Key Generated Error: the key passed is weak");
					break;
				default:
					break;
			}
		}
	}

	bool DES_SYSTEM::set_key(unsigned char _key[8], bool key_checked){
		memcpy(key, _key, 8);

		if(!key_checked){
			DES_set_key_unchecked((const_DES_cblock*)key, &key_schedule);
		}else{
			int ret = DES_set_key_checked((const_DES_cblock*)key, &key_schedule);
			switch(ret){
				case -1:
					return false;
					break;
				case -2:
					return false;
					break;
				default:
					return true;
					break;
			}
		}
		return true;
	}

	void DES_SYSTEM::DES_ECB_BLOCK_ENCRYPT(unsigned char in[8], unsigned char out[8], bool is_enc){
		if(mode != ecb){
			throw std::runtime_error(" please initialize the encryption mode as ecb mode\n");
		}
			
		DES_ecb_encrypt((const_DES_cblock*)in, (DES_cblock*)out, &key_schedule, is_enc);
	}

	void DES_SYSTEM::TEST_DES_ECB_BLOCK_ENCRYPT(unsigned char input[8], unsigned char output[8], bool is_enc, bool ip_covered, bool f_permutated, int F_round){
		if(mode != simpler_ecb){
			throw std::runtime_error(" please initialize the encryption mode as ecb mode\n");
		}

		/*
		*	int-type representation
		*/
		unsigned int l;
		unsigned int r;
		/*
		*	byte-type representation
		*/
		const unsigned char *in = input;
    	unsigned char *out = output;

    	/*
		*	byte-type convert to int-type, swap first right to left; left to right
		*/
    	c2l(in, r);
	    c2l(in, l);

	    /*********************************************Encryption routine*********************************/
	    // intial permutation
	    if(ip_covered){
	    	IP(r, l);
	    }
	    

	    /*
	    *	Make the permutation of the F function outside the loop
	    *
	    * 	clear the top bits on machines with 8byte longs 
	    * 	shift left by 2 
	    */
	    if(f_permutated){
	    	r = ROTATE(r, 29) & 0xffffffffL;
	    	l = ROTATE(l, 29) & 0xffffffffL;
	    }
	    

	    // extract the wrappered key
	    unsigned int* s = key_schedule.ks->deslong;

	    // cout<<"[I] to xor with secrete num is "<<r<<endl;

	    /*
	    *	core encryption and decryption routine
	    */
	    if(is_enc){
	    	for(int k=0; k<F_round; k++){
	    		if(k%2 == 0){
	    			D_ENCRYPT(l, r, 2*k);     /* even round */
				    // 			unsigned int u;
				    // 			unsigned int t;
				    // 			LOAD_DATA(r,2*k,u,t);
				    // 			t=ROTATE(t,4);
					/* 
					  unsigned int tmp =DES_SPtrans[0][(u>> 2L)&0x3f]^ \
					 		            DES_SPtrans[2][(u>>10L)&0x3f]^ \
					 		            DES_SPtrans[4][(u>>18L)&0x3f]^ \
							            DES_SPtrans[6][(u>>26L)&0x3f]^ \
					 		            DES_SPtrans[1][(t>> 2L)&0x3f]^ \
							            DES_SPtrans[3][(t>>10L)&0x3f]^ \
					 		            DES_SPtrans[5][(t>>18L)&0x3f]^ \
					 		            DES_SPtrans[7][(t>>26L)&0x3f];
					*/


					// l ^= tmp;
	    		}else{
	    			D_ENCRYPT(r, l, 2*k);     /* odd round */
	    		}
	    		
	    	}
	    }else{
	    	for(int k=F_round-1; k>=0; k--){
	    		if(k%2 == 0){
	    			D_ENCRYPT(r, l, 2*k);     /* even round */
	    		}else{
	    			D_ENCRYPT(l, r, 2*k);     /* odd round */
	    		}
	    	}
	    }


	    /* 
	    *	rotate and clear the top bits on machines with 8byte longs 
	    */
	    if(f_permutated){
	    	l = ROTATE(l, 3) & 0xffffffffL;
    		r = ROTATE(r, 3) & 0xffffffffL;
	    }
    	

	    // final permutation
	    if(ip_covered){
	    	FP(r, l);
	    }
	    
	    /*********************************************Encryption routine*********************************/
	    // cout<<"[O] output is \t"<<"("<< l <<","<< r << ")\n";
	    /*
		*	long-type convert to byte-type
		*/
	    l2c(l, out);
	    l2c(r, out);
	}

	unsigned char* DES_SYSTEM::DES_ECB_ENCRYPT(unsigned char* in, int& in_len, int& out_len, bool is_enc){
		if(mode != ecb){
			throw std::runtime_error(" please initialize the encryption mode as ecb mode\n");
		}
		/*
		*	Preprocessing
		*/
		int len = in_len;
		if(is_enc){
			len = ceil((double) (len+1)/8);
		}else{
			len = ceil((double) (len)/8);
		}
		
		unsigned char* src = new unsigned char[len*8];
		memcpy(src, in, in_len);	// copy from in

		if(is_enc){
			src[in_len] = 0xff;	// end mask
			unsigned char ch =	8 - (in_len+1) % 8; // PKCS7 padded 
			if(ch != 8){
				memset(src+in_len+1, ch, ch); // padded 
			}
		}


		/*
		*	Encryption or Decryption
		*/

		unsigned char* des = new unsigned char[len*8];
		unsigned char tmp_in[8] = {0};
		unsigned char tmp_out[8] = {0};
		for(int i=0; i<len; i++){
			memcpy(tmp_in, src + 8 * i, 8);
			DES_ecb_encrypt((const_DES_cblock*)tmp_in, (DES_cblock*)tmp_out, &key_schedule, is_enc);
			memcpy(des + 8 * i, tmp_out, 8);
		}
		out_len = len*8;

		if(! is_enc){
			// remove the padded and end mask
			unsigned char ch = des[out_len-1];
			if(ch == 0xff){
				ch = 1;
			}else{
				ch += 1;
			}
			out_len -= ch;

			unsigned char* tmp_des = new unsigned char[out_len];
			memcpy(tmp_des, des, out_len);
			delete des;
			des = tmp_des;
		}
		delete src;
		return des;
	}


	void DES_SYSTEM::get_key(unsigned char out[8]){
		if(mode == invalid){
			throw std::runtime_error(" please initialize the encryption mode before using this function\n");
		}
		
		memcpy(out, key, 8);
	}

	void DES_SYSTEM::get_key(unsigned int out[2]){
		unsigned char tmp[8];
		get_key(tmp);
		const unsigned char *in = tmp;

		c2l(in, out[0]);
	    c2l(in, out[1]);
	}

	void DES_SYSTEM::get_subkey(unsigned char out[16][8]){
		if(mode == invalid){
			throw std::runtime_error(" please initialize the encryption mode before using this function\n");
		}

		for(int i=0; i<16; i++){
			get_subkey(i, out[i]);
		}
	}

	void DES_SYSTEM::get_subkey(int round, unsigned char out[8]){
		// extract the wrappered key
		unsigned int* s = key_schedule.ks->deslong;

		unsigned int k0 = s[2*round];
		unsigned int k1 = s[2*round + 1];

		l2c(k0, out);
		l2c(k1, out);
	}


	void DES_SYSTEM::get_subkey(int round, unsigned int  out[2]){
		unsigned int* s = key_schedule.ks->deslong;

		out[0] = s[2*round];
		out[1] = s[2*round + 1];
	}

	void KEY_Simulator::get_key(unsigned char out[8]){
		memcpy(out, key, 8);
	}

	void KEY_Simulator::get_subkey(unsigned char out[16][8]){
		for(int i=0; i<16; i++){
			get_subkey(i, out[i]);
		}
	}

	void KEY_Simulator::get_subkey(int round, unsigned char out[8]){
		// extract the wrappered key
		unsigned int* s = subkeys;

		unsigned int k0 = s[2*round];
		unsigned int k1 = s[2*round + 1];

		l2c(k0, out);
		l2c(k1, out);
	}

	KEY_Simulator::KEY_Simulator(unsigned char _key[8]){
		memcpy(key, _key, 8);

		set_subkeys();
	}

	void KEY_Simulator::set_subkeys(){
		static const int shifts2[16] =
        { 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 };

        unsigned int c, d, t, s, t2;
    	const unsigned char *in = key;
    	unsigned int *k = subkeys;
    	int i;

    	c2l(in, c);
    	c2l(in, d);

    	/*
	     * do PC1 in 47 simple operations. Thanks to John Fletcher
	     * for the inspiration.
	     */
	    PERM_OP(d, c, t, 4, 0x0f0f0f0fL);
	    HPERM_OP(c, t, -2, 0xcccc0000L);
	    HPERM_OP(d, t, -2, 0xcccc0000L);
	    PERM_OP(d, c, t, 1, 0x55555555L);
	    PERM_OP(c, d, t, 8, 0x00ff00ffL);
	    PERM_OP(d, c, t, 1, 0x55555555L);
	    d = (((d & 0x000000ffL) << 16L) | (d & 0x0000ff00L) |
	         ((d & 0x00ff0000L) >> 16L) | ((c & 0xf0000000L) >> 4L));
	    c &= 0x0fffffffL;

	    for (i = 0; i < 16; i++) {
	        if (shifts2[i]) {
	            c = ((c >> 2L) | (c << 26L));
	            d = ((d >> 2L) | (d << 26L));
	        } else {
	            c = ((c >> 1L) | (c << 27L));
	            d = ((d >> 1L) | (d << 27L));
	        }
	        c &= 0x0fffffffL;
	        d &= 0x0fffffffL;
	        /*
	         * do PC2
	         */
	        s = des_skb[0][(c) & 0x3f] |
	            des_skb[1][((c >> 6L) & 0x03) | ((c >> 7L) & 0x3c)] |
	            des_skb[2][((c >> 13L) & 0x0f) | ((c >> 14L) & 0x30)] |
	            des_skb[3][((c >> 20L) & 0x01) | ((c >> 21L) & 0x06) |
	                       ((c >> 22L) & 0x38)];
	        t = des_skb[4][(d) & 0x3f] |
	            des_skb[5][((d >> 7L) & 0x03) | ((d >> 8L) & 0x3c)] |
	            des_skb[6][(d >> 15L) & 0x3f] |
	            des_skb[7][((d >> 21L) & 0x0f) | ((d >> 22L) & 0x30)];

	        /* table contained 0213 4657 */
	        t2 = ((t << 16L) | (s & 0x0000ffffL)) & 0xffffffffL;
	        *(k++) = ROTATE(t2, 30) & 0xffffffffL;

	        t2 = ((s >> 16L) | (t & 0xffff0000L));
	        *(k++) = ROTATE(t2, 26) & 0xffffffffL;
	    }
	}


	void KEY_Simulator::tmp_experiment(int round, unsigned int origin[2], unsigned int recover[2]){
		static const int shifts2[16] =
        { 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 };

        unsigned int c, d, t, s, t2;
    	const unsigned char *in = key;
    	unsigned int k[2];
    	int i;

    	c2l(in, c);
    	c2l(in, d);

    	/*
	     * do PC1 in 47 simple operations. Thanks to John Fletcher
	     * for the inspiration.
	     */
    	origin[0] = c;
    	origin[1] = d;
    	
	    PERM_OP(d, c, t, 4, 0x0f0f0f0fL);
	    HPERM_OP(c, t, -2, 0xcccc0000L);
	    HPERM_OP(d, t, -2, 0xcccc0000L);
	    PERM_OP(d, c, t, 1, 0x55555555L);
	    PERM_OP(c, d, t, 8, 0x00ff00ffL);
	    PERM_OP(d, c, t, 1, 0x55555555L);


	    d = (((d & 0x000000ffL) << 16L) | (d & 0x0000ff00L) |
	         ((d & 0x00ff0000L) >> 16L) | ((c & 0xf0000000L) >> 4L));
	    c &= 0x0fffffffL;

	    for (i = 0; i < round; i++) {
	    	// rotate for a 28-bits number
	        if (shifts2[i]) {
	            c = ((c >> 2L) | (c << 26L));
	            d = ((d >> 2L) | (d << 26L));
	        } else {
	            c = ((c >> 1L) | (c << 27L));
	            d = ((d >> 1L) | (d << 27L));
	        }
	        c &= 0x0fffffffL;
	        d &= 0x0fffffffL;
	        /*
	         * do PC2
	         */

	        // for s : the 9th, 18th, 22th, 25th bits are discarded 
	        s = des_skb[0][(c) & 0x3f] |
	            des_skb[1][((c >> 6L) & 0x03) | ((c >> 7L) & 0x3c)] |
	            des_skb[2][((c >> 13L) & 0x0f) | ((c >> 14L) & 0x30)] |
	            des_skb[3][((c >> 20L) & 0x01) | ((c >> 21L) & 0x06) |
	                       ((c >> 22L) & 0x38)];

	        // for t : the 7th, 10th, 15th, 26th bits are discarded
	        t = des_skb[4][(d) & 0x3f] |
	            des_skb[5][((d >> 7L) & 0x03) | ((d >> 8L) & 0x3c)] |
	            des_skb[6][(d >> 15L) & 0x3f] |
	            des_skb[7][((d >> 21L) & 0x0f) | ((d >> 22L) & 0x30)];

	        /* table contained 0213 4657 */
	        t2 = ((t << 16L) | (s & 0x0000ffffL)) & 0xffffffffL;
	        k[0] = ROTATE(t2, 30) & 0xffffffffL;

	        t2 = ((s >> 16L) | (t & 0xffff0000L));
	        k[1] = ROTATE(t2, 26) & 0xffffffffL;
	    }

	    inverse_subkeys(round, k, recover);
	}

	void DES_Cracker::load_xor_distribution_map(){
		unsigned int S_BOX[8][64];
	    for(int i=0; i<8; i++){
	        memcpy(S_BOX[i], DES_SPtrans[i], 64*sizeof(unsigned int));
	    }

	    unsigned int b;
	    unsigned int output_xor;
	    unsigned int bit;
	    unsigned int tmp;

		for(int No_BOX = 0; No_BOX<8; No_BOX++){
	        for(unsigned int input_xor=0; input_xor<64; input_xor++){
	            for(unsigned int a=0; a<64; a++){
	                b = a^input_xor;
	                output_xor = S_BOX[No_BOX][a] ^ S_BOX[No_BOX][b];

	                tmp = 0;
	                for(int i=0; i<4; i++){
	                    bit = ((output_xor >> DES_S_index[No_BOX][i]) & 1);
	                    tmp |= bit<<i;
	                }
	                output_xor = tmp;
	                xor_distribution_map[No_BOX][input_xor][output_xor].insert(a);
	                xor_distribution_map[No_BOX][input_xor][output_xor].insert(b);
	            }
	        }
	    }
	}

	DES_Cracker::DES_Cracker(DES_SYSTEM* _victim){
		load_xor_distribution_map();

		victim = _victim;
	}

	bool DES_Cracker::attack_ecb_des(int round, unsigned char simulation_key[8]){
		switch(round){
			case 1:
				return attack_1round_ecb_des(simulation_key);
				break;
			case 3:
				return attack_3round_ecb_des(simulation_key);
				break;
			default:
				return false;
				break;
		}

		return false;
	}

	void DES_Cracker::parse_xor_output(unsigned int x, unsigned int out_xor[8]){
		unsigned int bit;
    	unsigned int tmp;

		for(int No_BOX=0; No_BOX<8; No_BOX++){
			tmp = 0;
            for(int i=0; i<4; i++){
                bit = ((x >> DES_S_index[No_BOX][i]) & 1);
                tmp |= bit<<i;
            }
            out_xor[No_BOX] = tmp;
		}
	}

	void DES_Cracker::parse_xor_input(const unsigned int& x, unsigned int in_xor[8]){
		unsigned int u = x;
		unsigned int t = ROTATE(x,4);
		in_xor[0] = (u>>2L)  & 0x3f;
		in_xor[2] = (u>>10L) & 0x3f;
		in_xor[4] = (u>>18L) & 0x3f;
		in_xor[6] = (u>>26L) & 0x3f;
		in_xor[1] = (t>>2L)  & 0x3f;
		in_xor[3] = (t>>10L) & 0x3f;
		in_xor[5] = (t>>18L) & 0x3f;
		in_xor[7] = (t>>26L) & 0x3f;
	}

	void DES_SYSTEM::get_S_BOX_subkey(int round, unsigned int out[8]){
		unsigned int k[2];
		get_subkey(round, k);

		unsigned int u = k[0];
		unsigned int t = ROTATE(k[1],4);
		out[0] = (u>>2L)  & 0x3f;
		out[2] = (u>>10L) & 0x3f;
		out[4] = (u>>18L) & 0x3f;
		out[6] = (u>>26L) & 0x3f;
		out[1] = (t>>2L)  & 0x3f;
		out[3] = (t>>10L) & 0x3f;
		out[5] = (t>>18L) & 0x3f;
		out[7] = (t>>26L) & 0x3f;
	}
	bool DES_Cracker::attack_1round_ecb_des(unsigned char simulation_key[8]){
		unsigned char input[2][8]; 
    	unsigned char output[2][8];
    	unsigned char *in ;
    	unsigned char *out ;

    	unsigned int round1_subkeys[8];

    	/*
    	*	For each pair
    	*		index 0 for left of the first element
    	*		index 1 for right of the first element
    	*		index 2 for left of the second element
    	*		index 3 for right of the second element
    	*/
    	const unsigned int max_pairs = 1000;
    	unsigned int in_xor_pair[max_pairs][4];
    	unsigned int out_xor_pair[max_pairs][4];

    	unsigned int in_xor_origin[max_pairs];
    	unsigned int out_xor_origin[max_pairs];

    	unsigned int in_xor_S[max_pairs][8];
    	unsigned int in_S[max_pairs][8];
    	unsigned int out_xor_S[max_pairs][8];

    	unsigned int counter_Matrix[8][64];
    	for(int i=0; i<8; i++){
    		for(int j=0; j<64; j++){
    			counter_Matrix[i][j] = 0;
    		}
    	}
  		// use for generating random input
    	srand((unsigned)time(NULL));

    	for(int No_pair =0; No_pair<64; No_pair ++){
    		/*
	    	*	For the one round DES; the left part is the true xor input
	    	*/
	    	// bin : 00011001 11100000 11100111 00001001 oct : 434169609
	    	// bin : 01100011 01101010 00110001 00011010 oct : 1667903770
	    	in_xor_pair[No_pair][0] = rand(); in_xor_pair[No_pair][1] = 0;
	    	in_xor_pair[No_pair][2] = rand(); in_xor_pair[No_pair][3] = 0;

	    	/*
	    	*	 After the execution
	    	*		For the first input of the encryption routine TEST_DES_ECB_BLOCK_ENCRYPT
	    	*		the S-box input  is in_xor_pair[No_pair][0] 
	    	*		the S-box output is out_xor_pair[No_pair][0] 
	    	*/
	    	in = input[0];
	    	l2c(in_xor_pair[No_pair][0], in);
		    l2c(in_xor_pair[No_pair][1], in);
			victim->TEST_DES_ECB_BLOCK_ENCRYPT(input[0], output[0], true, false, false, 1);
			out = output[0];
			c2l(out, out_xor_pair[No_pair][0]);
			c2l(out, out_xor_pair[No_pair][1]);
			out_xor_pair[No_pair][0] ^= in_xor_pair[No_pair][1];


			/*
	    	*	 After the execution
	    	*		For the second input of the encryption routine TEST_DES_ECB_BLOCK_ENCRYPT
	    	*		the S-box input  is in_xor_pair[No_pair][2] 
	    	*		the S-box output is out_xor_pair[No_pair][2] 
	    	*/
			in = input[1];
			l2c(in_xor_pair[No_pair][2], in);
		    l2c(in_xor_pair[No_pair][3], in);
			victim->TEST_DES_ECB_BLOCK_ENCRYPT(input[1], output[1], true, false, false, 1);
			out = output[1];
			c2l(out, out_xor_pair[No_pair][2]);
			c2l(out, out_xor_pair[No_pair][3]);
			out_xor_pair[No_pair][2] ^= in_xor_pair[No_pair][3];


			/*
			*	Begin the crypto analysis
			*		1) calculate the xor     of the S-BOX
			*		2) calculate the in-xor  for each S-BOX
			*		3) calculate the out-xor for each S-BOX
			*/
			in_xor_origin[No_pair] = in_xor_pair[No_pair][0]^in_xor_pair[No_pair][2];
			out_xor_origin[No_pair] = out_xor_pair[No_pair][0]^out_xor_pair[No_pair][2];
			parse_xor_input(in_xor_origin[No_pair], in_xor_S[No_pair]);
			parse_xor_input(in_xor_pair[No_pair][0], in_S[No_pair]);
			parse_xor_output(out_xor_origin[No_pair], out_xor_S[No_pair]);


			/*
			*	For each S_BOX construct the counter Matrix
			*/
			for(int No_BOX=0; No_BOX<8; No_BOX++){
				set<unsigned int>& candidate = xor_distribution_map[No_BOX][in_xor_S[No_pair][No_BOX]][out_xor_S[No_pair][No_BOX]];
				for(set<unsigned int>::iterator it=candidate.begin() ;it!=candidate.end();it++)
			    {
			        counter_Matrix[No_BOX][(*it)^in_S[No_pair][No_BOX]] ++;
			    }
			}
    	}

    	/*
    	*	recover s-box key has been calculated and stored in round1_subkeys
    	*/
    	for(int No_BOX=0; No_BOX<8; No_BOX++){
    		for(int i=0; i<64; i++){
    			if(counter_Matrix[No_BOX][i] == 64){
    				round1_subkeys[No_BOX] = i;
    				break;
    			}
    		}
    	}


    	/*
    	*	recover round 1 two 48-bits subkey has been calculated and stored in kk[2]
    	*/
    	unsigned int k[2] = {0};
    	k[0] = (((round1_subkeys[0] & 0x3f) << 2L) | k[0]);
    	k[0] = (((round1_subkeys[2] & 0x3f) << 10L) | k[0]);
    	k[0] = (((round1_subkeys[4] & 0x3f) << 18L) | k[0]);
    	k[0] = (((round1_subkeys[6] & 0x3f) << 26L) | k[0]);

    	k[1] = (((round1_subkeys[1] & 0x3f) << 2L) | k[1]);
    	k[1] = (((round1_subkeys[3] & 0x3f) << 10L) | k[1]);
    	k[1] = (((round1_subkeys[5] & 0x3f) << 18L) | k[1]);
    	k[1] = (((round1_subkeys[7] & 0x3f) << 26L) | k[1]);
    	k[1] = ROTATE(k[1],28);

    	cout<<"recover subkey complete:\tround 1 two subkey ("<<k[0]<<","<<k[1]<<")\n";


    	unsigned int kk[2] = {k[0], k[1]};
    	DES_SYSTEM_SPACE::inverse_subkeys(1, kk, kk);

    	/*
    	*	recover the origin 56-bits key
    	*/
    	unsigned char standard_input[8];
    	unsigned char standard_output[8];
    	unsigned char test_output[8];
    	unsigned char check_key[8];
    	ffse::randomSketchGenerator rsg;
		rsg.reseed();
		bool ret = 1;

		// randomly generate the weak key bytes
		ret = rsg.random_bytes(8, (unsigned char *)standard_input);
		if(!ret){
			throw std::runtime_error("Key Generated Error: cannot generate random bytes");
		}
    	victim->TEST_DES_ECB_BLOCK_ENCRYPT(standard_input, standard_output, true, true, true, 16);


    	//(0  [1]  [2]  8  [12]  [13]  16  24     ,       0  8  [10]  [13]  16  [20]  [22]  24  )
    	for(unsigned int i = 0; i<256; i++){
    		unsigned int tmp  = 0;
    		unsigned int mask = 0;
    		unsigned int candidate = i;
    		tmp = 0xffffcff9L;
    		mask = 0x00003006L;
    		tmp = (((candidate & 0x03) << 1L) | tmp);
    		tmp = (((candidate & 0x0c) << 10L) | tmp);
    		
    		k[0] = kk[0];
    		k[0] = (k[0] | mask);
    		k[0] = (k[0] & tmp);

    		candidate = ((candidate & 0xf0) >> 4);
    		tmp  = 0xffafdbffL;
    		mask = 0x00502400L;
    		tmp = (((candidate & 0x01) << 10L) | tmp);
    		tmp = (((candidate & 0x02) << 12L) | tmp);
    		tmp = (((candidate & 0x04) << 18L) | tmp);
    		tmp = (((candidate & 0x08) << 19L) | tmp);

    		k[1] = kk[1];
    		k[1] = (k[1] | mask);
    		k[1] = (k[1] & tmp);


    		in = check_key;
    		l2c(k[0],in);
    		l2c(k[1],in);
    		if(victim->set_key(check_key)){
    			victim->TEST_DES_ECB_BLOCK_ENCRYPT(standard_input, test_output, true, true, true, 16);
    			ret = false;
    			for(int i=0; i<8; i++){
    				ret |= (test_output[i] - standard_output[i]);
    			}

    			if(! ret ){
    				memcpy(simulation_key, check_key, 8);
    				cout<<"recover the key schedule part complete:\t\n";
    				return true;
    				break;
    			}
    		}
    	}

    	return false;

	}

	bool DES_Cracker::attack_3round_ecb_des(unsigned char simulation_key[8]){
		unsigned char input[2][8];
    	unsigned char output[2][8];
    	unsigned char *in ;
    	unsigned char *out ;

    	unsigned int round3_subkeys[8];

    	/*
    	*	For each pair
    	*		index 0 for left of the first element
    	*		index 1 for right of the first element
    	*		index 2 for left of the second element
    	*		index 3 for right of the second element
    	*/
    	const unsigned int max_pairs = 1000;
    	unsigned int in_xor_pair[max_pairs][4];
    	unsigned int out_xor_pair[max_pairs][4];

    	unsigned int in_xor_origin[max_pairs];
    	unsigned int out_xor_origin[max_pairs];

    	unsigned int in_xor_S[max_pairs][8];
    	unsigned int in_S[max_pairs][8];
    	unsigned int out_xor_S[max_pairs][8];

    	unsigned int counter_Matrix[8][64];
    	for(int i=0; i<8; i++){
    		for(int j=0; j<64; j++){
    			counter_Matrix[i][j] = 0;
    		}
    	}
  		// use for generating random input
    	srand((unsigned)time(NULL));

    	for(int No_pair =0; No_pair<64; No_pair ++){
    		/*
	    	*	For the three round DES; 
	    	*		1)	in the first round, the left part is the true xor input
	    	*		2)	we hope the the left parts are the same for the two input elements
	    	*	
	    	*/
	    	// bin : 00011001 11100000 11100111 00001001 oct : 434169609
	    	// bin : 01100011 01101010 00110001 00011010 oct : 1667903770
	    	in_xor_pair[No_pair][0] = 0; in_xor_pair[No_pair][1] = rand();
	    	in_xor_pair[No_pair][2] = 0; in_xor_pair[No_pair][3] = rand();

	    	/*
	    	*	 After the execution
	    	*		For the first input of the encryption routine TEST_DES_ECB_BLOCK_ENCRYPT 
	    	*/
	    	in = input[0];
	    	l2c(in_xor_pair[No_pair][0], in);
		    l2c(in_xor_pair[No_pair][1], in);
			victim->TEST_DES_ECB_BLOCK_ENCRYPT(input[0], output[0], true, false, false, 3);
			out = output[0];
			c2l(out, out_xor_pair[No_pair][0]); //roud-3 l
			c2l(out, out_xor_pair[No_pair][1]);	//roud-3 r


			/*
	    	*	 After the execution
	    	*		For the second input of the encryption routine TEST_DES_ECB_BLOCK_ENCRYPT
	    	*/
			in = input[1];
			l2c(in_xor_pair[No_pair][2], in);
		    l2c(in_xor_pair[No_pair][3], in);
			victim->TEST_DES_ECB_BLOCK_ENCRYPT(input[1], output[1], true, false, false, 3);
			out = output[1];
			c2l(out, out_xor_pair[No_pair][2]);
			c2l(out, out_xor_pair[No_pair][3]);


			/*
			*	Begin the crypto analysis
			*		1) calculate the out-xor     of the 3 round S-BOX i.e. l1^l1*^l3^l3* = l0^l0*^l3^l3*
			*		   calculate the in-xor 	 of the 3 round S-BOX i.e. r3^r3*			
			*		2) calculate the in-xor  for each S-BOX
			*		3) calculate the out-xor for each S-BOX
			*/
			in_xor_origin[No_pair] =  out_xor_pair[No_pair][1]^out_xor_pair[No_pair][3];
			out_xor_origin[No_pair] = in_xor_pair[No_pair][1]^in_xor_pair[No_pair][3]^ \
									  out_xor_pair[No_pair][0]^out_xor_pair[No_pair][2];
			parse_xor_input(in_xor_origin[No_pair], in_xor_S[No_pair]);
			parse_xor_input(out_xor_pair[No_pair][1], in_S[No_pair]);
			parse_xor_output(out_xor_origin[No_pair], out_xor_S[No_pair]);


			/*
			*	For each S_BOX construct the counter Matrix
			*/
			for(int No_BOX=0; No_BOX<8; No_BOX++){
				set<unsigned int>& candidate = xor_distribution_map[No_BOX][in_xor_S[No_pair][No_BOX]][out_xor_S[No_pair][No_BOX]];
				for(set<unsigned int>::iterator it=candidate.begin() ;it!=candidate.end();it++)
			    {
			        counter_Matrix[No_BOX][(*it)^in_S[No_pair][No_BOX]] ++;
			    }
			}
    	}

    	for(int No_BOX=0; No_BOX<8; No_BOX++){
    		for(int i=0; i<64; i++){
    			if(counter_Matrix[No_BOX][i] == 64){
    				round3_subkeys[No_BOX] = i;
    				break;
    			}
    		}
    	}



    	unsigned int k[2] = {0};
    	k[0] = (((round3_subkeys[0] & 0x3f) << 2L) | k[0]);
    	k[0] = (((round3_subkeys[2] & 0x3f) << 10L) | k[0]);
    	k[0] = (((round3_subkeys[4] & 0x3f) << 18L) | k[0]);
    	k[0] = (((round3_subkeys[6] & 0x3f) << 26L) | k[0]);

    	k[1] = (((round3_subkeys[1] & 0x3f) << 2L) | k[1]);
    	k[1] = (((round3_subkeys[3] & 0x3f) << 10L) | k[1]);
    	k[1] = (((round3_subkeys[5] & 0x3f) << 18L) | k[1]);
    	k[1] = (((round3_subkeys[7] & 0x3f) << 26L) | k[1]);
    	k[1] = ROTATE(k[1],28);

    	cout<<"recover subkey complete:\tround 3 two subkey ("<<k[0]<<","<<k[1]<<")\n";

    	unsigned int kk[2] = {k[0], k[1]};
    	DES_SYSTEM_SPACE::inverse_subkeys(3, kk, kk);

    	/*
    	*	recover the origin 56-bits key
    	*/
    	unsigned char standard_input[8];
    	unsigned char standard_output[8];
    	unsigned char test_output[8];
    	unsigned char check_key[8];
    	ffse::randomSketchGenerator rsg;
		rsg.reseed();
		bool ret = 1;

		// randomly generate the weak key bytes
		ret = rsg.random_bytes(8, (unsigned char *)standard_input);
		if(!ret){
			throw std::runtime_error("Key Generated Error: cannot generate random bytes");
		}
    	victim->TEST_DES_ECB_BLOCK_ENCRYPT(standard_input, standard_output, true, true, true, 16);


    	//(0  8  16  [18]  [21]  24  [30]       ,       0  8  [10]  [11]  16  [17]  [20]  24  [31]  )
    	for(unsigned int i = 0; i<256; i++){
    		unsigned int tmp  = 0;
    		unsigned int mask = 0;
    		unsigned int candidate = i;
    		tmp  = 0xbfdbffffL;
    		mask = 0x40240000L;
    		tmp = (((candidate & 0x01) << 18L) | tmp);
    		tmp = (((candidate & 0x02) << 20L) | tmp);
    		tmp = (((candidate & 0x04) << 28L) | tmp);

    		
    		k[0] = kk[0];
    		k[0] = (k[0] | mask);
    		k[0] = (k[0] & tmp);

    		candidate = ((candidate & 0xf8) >> 3);
    		tmp  = 0x7fedf3ffL;
    		mask = 0x80120c00L;
    		tmp = (((candidate & 0x03) << 10L) | tmp);
    		tmp = (((candidate & 0x04) << 15L) | tmp);
    		tmp = (((candidate & 0x08) << 17L) | tmp);
    		tmp = (((candidate & 0x10) << 27L) | tmp);


    		k[1] = kk[1];
    		k[1] = (k[1] | mask);
    		k[1] = (k[1] & tmp);


    		

    		in = check_key;
    		l2c(k[0],in);
    		l2c(k[1],in);
    		if(victim->set_key(check_key)){
    			victim->TEST_DES_ECB_BLOCK_ENCRYPT(standard_input, test_output, true, true, true, 16);
    			ret = false;
    			for(int i=0; i<8; i++){
    				ret |= (test_output[i] - standard_output[i]);
    			}

    			if(! ret ){
    				memcpy(simulation_key, check_key, 8);
    				cout<<"recover the key schedule part complete:\t\n";
    				return true;
    				break;
    			}
    		}
    	}

    	return false;
	}

	void parse_skb_output(unsigned int x, unsigned int y,unsigned int out_xor[8]){
		unsigned int bit;
    	unsigned int tmp;

		for(int No_BOX=0; No_BOX<4; No_BOX++){
			tmp = 0;
            for(int i=0; i<6; i++){
                bit = ((x >> DES_skb_index[No_BOX][i]) & 1);
                tmp |= bit<<i;
            }
            out_xor[No_BOX] = tmp;
		}

		for(int No_BOX=4; No_BOX<8; No_BOX++){
			tmp = 0;
            for(int i=0; i<6; i++){
                bit = ((y >> DES_skb_index[No_BOX][i]) & 1);
                tmp |= bit<<i;
            }
            out_xor[No_BOX] = tmp;
		}
	}
	void inverse_subkeys(int round, unsigned int subkey[2], unsigned int result[2]){
		static const int shifts2[16] =
        { 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 };

		unsigned int k0 = subkey[0];
		unsigned int k1 = subkey[1];

		/*
		*	Inverse PC2
		*/
		unsigned int s = 0;
		unsigned int t = 0;
		unsigned int tmp = 0;
		tmp = ROTATE(k0, 2) & 0xffffffffL;
		s = (tmp & 0x0000ffffL);
		t = (tmp >> 16);
		tmp = ROTATE(k1, 6) & 0xffffffffL;
		s = (((tmp & 0x0000ffffL) << 16) | s);
		t = (((tmp >> 16) << 16) | t);

		unsigned int skb_output[8];
		unsigned int cd_slices[8];
		parse_skb_output(s, t, skb_output);
		for(int i=0; i<8; i++){
			cd_slices[i] = DES_skb_inverse[i][skb_output[i]];
		}

		unsigned int c = 0;
		unsigned int d = 0;
		c =  (cd_slices[0] & 0x3f) ;
		c = ((cd_slices[1] & 0x03) << 6L) | c;
		c = ((cd_slices[1] & 0x3c) << 7L) | c;
		c = ((cd_slices[2] & 0x0f) << 13L)	| c;
		c = ((cd_slices[2] & 0x30) << 14L)	| c;
		c = ((cd_slices[3] & 0x01) << 20L)	| c;
		c = ((cd_slices[3] & 0x06) << 21L)	| c;
		c = ((cd_slices[3] & 0x38) << 22L)	| c;

		d =  (cd_slices[4] & 0x3f) ;
		d = ((cd_slices[5] & 0x03) << 7L) | d;
		d = ((cd_slices[5] & 0x3c) << 8L) | d;
		d = ((cd_slices[6] & 0x3f) << 15L) | d;
		d = ((cd_slices[7] & 0x0f) << 21L) | d;
		d = ((cd_slices[7] & 0x30) << 22L) | d;

		for(int i=0; i<round; i++){
			// rotate for a 28-bits number
	        if (shifts2[i]) {
	            c = ((c << 2L) | (c >> 26L));
	            d = ((d << 2L) | (d >> 26L));
	        } else {
	            c = ((c << 1L) | (c >> 27L));
	            d = ((d << 1L) | (d >> 27L));
	        }
	        c &= 0x0fffffffL;
	        d &= 0x0fffffffL;
		}

		c = (c | ((d & 0x0f000000L) << 4L)); 
		d = (((d & 0x000000ffL) << 16L) | (d & 0x0000ff00L) | ((d & 0x00ff0000L) >> 16L) );


		PERM_OP(d, c, t, 1, 0x55555555L);
	    PERM_OP(c, d, t, 8, 0x00ff00ffL);
	    PERM_OP(d, c, t, 1, 0x55555555L);
	    HPERM_OP(d, t, -2, 0xcccc0000L);
	    HPERM_OP(c, t, -2, 0xcccc0000L);
	    PERM_OP(d, c, t, 4, 0x0f0f0f0fL);

	    result[0] = c;
	    result[1] = d;
	}
}

