/*************************************************************************
* qr_test.h
* Some auxiliary functiona and check functionality for the functionality of 
*	calculating the root of the quardratic residure of a given mod prime
*
* Author: stoneboat@mail.nankai.edu.cn 
* (C) 2019 University of NKU. Free for used
***********************************************************************/


#include <openssl/bn.h>
#include <openssl/err.h>
namespace OPENSSL{
	namespace BIGNUM_SPACE{
		/*
		*  	this function use the functionality BN_generate_prime_ex(BIGNUM *ret, int bits, int safe, const BIGNUM *add,
		     const BIGNUM *rem, BN_GENCB *cb) offered by openssl 1.1.0, generate a prime with least_bits_length with no more other additonal properties

		    it also use the functionality BN_is_prime_ex to further guarantee that the generated prime is valid
		*/
		BIGNUM* generate_normal_prime(unsigned int least_bits_length);

		/*
		*	generate a random quadratic residure when the p is mod_p
		*/
		BIGNUM*	generate_quadratic_residure(const BIGNUM* mod_p);

		/*
		*	generate a random non quadratic residure when the p is mod_p
		*/
		BIGNUM*	generate_non_quadratic_residure(const BIGNUM* mod_p);

		/*
		*	get the order of a element in 2 sylow group of Zp
		*	its order is 2^order, and the maximum likely order is 2^r, since the number of elements in the 2 sylow group is 2^r
		*/
		void get_order_of_2sylow_element(const BIGNUM* mod_p, const BIGNUM* b, BIGNUM* order);

		/*
		*	extract r and s in the form of p-1 = 2^r * s
		*/
		void extract_r_and_s(const BIGNUM* mod_p, BIGNUM* r, BIGNUM* s);

		/*
		*	print a openssl bignum in dec format
		*/
		void bn_dec_printf(BIGNUM * a);

		/*
		*	print a openssl bignum in hex format
		*/
		void bn_hex_printf(BIGNUM * a);

		/*
		*	generate the random prime with length of least_bits_length, quadratic residure and solve the result of the root of this specific
		*		quadratic residure
		*/
		void random_test_on_root_of_quadratic_residure(unsigned int least_bits_length);
	}
}